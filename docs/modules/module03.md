# III. IMPRESSION 3D - PrusaSlicer

Le troisième module consiste à nous former à l’utilisation des imprimantes 3D. La salle du fablab est équipée de plusieurs **Original Prusa i3 MK3S**, un modèle qui est fabriqué grace à l’acquisition de pièces majeurs qu’il faut ensuite monter soi-même. Je vais maintenant vous expliquer comment j’ai pu obtenir le prototype final de la chaise Nastro ;

1 / Tout d’abord il faut installer le logiciel pour raccorder l’objet modelé via fusion sur mac et l’imprimante 3D. Il est possible de télécharger ce logiciel sur le site de [Prusa3D](https://www.prusa3d.com/drivers/)
Attention, une fois télécharger (si celui-ci ne s'affiche pas) il est ensuite important de se rendre dans **Configuration > Assistant de configuration > Original Prusa i3 MK3S > 0.4 mm buse**

2 / Importer l’objet modélisé sur fusion 360 : pour ce faire il faut exporter le fichier en **.stl** (format supporté par l’imprimante) et ensuite l’ouvrir dans le logiciel PrusaSlicer : l’objet apparaît sur le plateau de l’imprimante 3D, si nécessaire il faut adapter l’échelle grâce aux outils à gauche. Il faudra ensuite trouver la face idéal à poser comme base (en ayant le minimum de contraintes possibles au niveaux des supports) La chaise nastro est un objet organique et courbe, je l’ai donc poser de manière à avoir la plus grande surface plate contre le plateau (l’assise).

3 / Réglages des paramètres d’impression : Vérifier que l’on se trouve bien dans le mode expert (en haut à droite, écrit en rouge) afin d’être plus précis dans les réglages supplémentaires. Ensuite dans **réglages d’impression** :

## Couches et périmètres

![](../images/Capture_d_écran_2020-10-27_à_16.11.36.png)

> (Notes :  A savoir que le filament utilisé au fablab effectue des couches à hauteur de 0.2mm et que la buse de l’imprimante 3D peut aller jusqu’a 0.4 mm d’épaisseur de trait afin de mieux comprendre le fonctionnement.)

Une fois que les réglages de **Couches et périmetres** sont encodés :

## Remplissage

![](../images/Capture_d_écran_2020-10-27_à_16.11.49.png)
 
La Nastro Chair ayant pour caractéristiques : souplesse, rigidité et légereté, j'ai opté pour un remplissage gyroïde car en me renseignement j'ai appris qu'il se trouvait etre plus léger et plus solide comparé aux remplissages nid d'abeilles ou rectilignes. Cependant le temps (3H36) et la quantité de matiere (9,34m) restaient tres proches d'un motifs à l'autre.

Une fois que les réglages de **Remplissage** sont encodés :

## Jupes et bordures

![](../images/Capture_d_écran_2020-10-27_à_16.12.03.png)

La largeur de la bordure dépend de l'objet. Ansi son epaisseur est en correlation direct avec le besoin d'ancrage au plateau qu'a besoin l'objet, plus l'objet est grand plus elle augmentera.

Une fois que les réglages de **Jupe et bordures** sont encodés : Il faudra prêter attention aux **Supports** lors de la confection de modeles tels que la Nastro Chair! En effet, de par sa conception accordant une importance au vide, le modele se voudra fragile a certain moment de sa confection. C'est d'ailleurs la raison pour laquelle ma premiere impression 3D fut un echec.. 

![](../images/fail.jpg)

> à 30% d'impression, le dossier de la chaise s'est brisé dû au frottements crées par les mouvements de la buse. `

Grâce à l'aide de Gwendoline, voici les modifications apportées aux réglages des **Supports** pour corriger ce problème : En cochant **support sur le plateau uniquement**, j'avais empêcher le soutien du porte-a-faux du dossier durant l'impression 3D : il fallait donc décocher cette case. En augmentant l'espacement de motif de 2 à 4 et l'espacements du motif d'interface de 0.4 à 2, ca me permettra d'enlever plus facilement les supports entre l'assise et le dossier : 

## Supports

![](../images/Capture_d_écran_2020-10-27_à_17.39.52.png)

Il faudra finalement exporter le **g-code** sur la carte sd, l'insérer dans la Prusa3D : **Sélectionner votre dossier > OK**. La buse commence à se déplacer, elle trace un premier trait en bas à gauche et transforme ensuite le modèle en réalité matériel. 

>1. Le plateau doit être nettoyer à l'acétone avant impression afin de garantir une adhérence optimale à l'objet.
>2. Il est primordial de surveiller la conception du prototype durant les 3 premières couches afin de s'assurer du bon déroulement de celle-ci.
>3. Prêter attention à la vitesse, ne pas dépasser 100% idéalement. Plus la machine est lente, plus le design de l'objet sera effectué de maniere consiencieuse.

## RESULTAT FINAL / Impression 3D

![](../images/Capture_d_écran_2020-10-27_à_19.48.38.png)

Mon choix s'est tourné vers la chaise Nastro pour sa forme intriguante et ses courbes : De sa modélisation 3D sur Fusion360 à son impression, cesont ses courbes qui m'ont fascinées. Son esthétique lui donnant l'allure d'une sculpture cache en réalité une assise bien réfléchie pour le comfort de l'homme. Mes attentes quant à cet exercice était de comprendre sa fabrication. C'est donc dans une démarche d'hommage à l'oeuvre de Leonardi que je me suis située. Je suis satisfaite de ce prototype qui atteint mes objectifs ; grâce aux différentes étapes de conception j'ai su reproduire les ombres générées par sa forme et j'ai appris à comprendre comment d'une forme simple ; le cylindre, on peut arriver à concevoir une forme abstraite et délicate.

![](../images/Capture_d_écran_2020-10-29_à_20.30.43.png)



