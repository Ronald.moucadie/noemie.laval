# I. FORMATION FABZERO - GitLab & Fusion 360

Ce jeudi nous avons commencé la **formation fabzero** ; Après avoir appris à manipuler le programme Fusion 360 par la modélisation d'objet, on a ensuite été initié au **coding CSS**. La création de fablab s'est développée par le MIT dans l'idée de former ses étudiants avec peu de moyen tout en permettant la formation d'un grand nombre d'outils. Plusieurs fablabs ont donc émergés un peu partout dans le monde pour finalement devenir une vraie communauté. Qui dit communauté, dit partage de conaissance ; Ainsi grace à la documentation des découvertes faites durant son aprentissage, on donne la possibilité a chacun d'en apprendre plus. Le coding est un moyen de partager ses conaissances d'autant plus rapidement dans le monde via la web.

Afin de démarrer ma page pour me présenter :

1. Je me suis rendue dans : Your groups -> **Fablab ULB/Enseignements/2020-2021/FabZero-Design/Fistname.Surname**. 
2. Une fois entrée dans : **docs/index.md**, je commence à rédiger ma page via le bouton edit, 
3. le même processus sera executé pour rédiger les différents modules, cette fois dans : **docs/modules**. 
4. Pour finir, dans : **docs/images**, j'upload mes photos (réduites a 1000px dans Ps) via le bouton **+**. 

Ci-dessous un tableau afin de résumer les différentes manieres d'éditer sa page GitLab : 

![](../images/Capture_d_écran_2020-10-29_à_18.54.19.png)

## MISE EN ROUTE DE GIT SUR **MAC**

**Git** est un terminal, c'est un **logiciel de commandes** qui permet d'effectuer différentes tâches via divers logicels sans devoir les ouvrir, c'est ce qu'on apelle aussi le coding, c'est donc un réel gain de temps dans le developpement informatique de sites, logiciels etc.

Après avoir suivi les indications de [ce tutoriel](https://github.com/Academany/fabzero/blob/master/program/basic/git.md), j'ai pu installer la derniere vesion git. Je n'ai pas été plus loin dans le processus d'assimilation du site à mon terminal mac car il me renvoyait a des autorisation pouvant mettre ce dernier en danger. Je préfere donc reporter cette étape au moment ou mon mac sera completement nettoyé.

![](../images/Capture_d_écran_2020-11-11_à_23.18.32.png)


